﻿namespace Examen2Parcial
{
    partial class Instalación
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.txtInformacion = new System.Windows.Forms.TextBox();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.cbxInstall = new System.Windows.Forms.ComboBox();
            this.cbxRedundancia = new System.Windows.Forms.ComboBox();
            this.btnCrear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkRed;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("MS PGothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(42, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(126, 49);
            this.button1.TabIndex = 0;
            this.button1.Text = "Personalizar Instalación";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtInformacion
            // 
            this.txtInformacion.Location = new System.Drawing.Point(257, 86);
            this.txtInformacion.Multiline = true;
            this.txtInformacion.Name = "txtInformacion";
            this.txtInformacion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInformacion.Size = new System.Drawing.Size(398, 337);
            this.txtInformacion.TabIndex = 1;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.DarkRed;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.Font = new System.Drawing.Font("MS PGothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Location = new System.Drawing.Point(571, 29);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(84, 31);
            this.btnLimpiar.TabIndex = 2;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DarkRed;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("MS PGothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(42, 164);
            this.button2.Name = "button2";
            this.button2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.button2.Size = new System.Drawing.Size(126, 49);
            this.button2.TabIndex = 3;
            this.button2.Text = "Instalación";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // cbxInstall
            // 
            this.cbxInstall.BackColor = System.Drawing.Color.DarkRed;
            this.cbxInstall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxInstall.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbxInstall.Font = new System.Drawing.Font("MS PGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxInstall.Items.AddRange(new object[] {
            "/-/-/-/",
            "FULL",
            "PART",
            "MIN"});
            this.cbxInstall.Location = new System.Drawing.Point(45, 253);
            this.cbxInstall.Name = "cbxInstall";
            this.cbxInstall.Size = new System.Drawing.Size(123, 28);
            this.cbxInstall.TabIndex = 7;
            this.cbxInstall.SelectedIndexChanged += new System.EventHandler(this.cbxInstall_SelectedIndexChanged);
            // 
            // cbxRedundancia
            // 
            this.cbxRedundancia.BackColor = System.Drawing.Color.DarkRed;
            this.cbxRedundancia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxRedundancia.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbxRedundancia.Font = new System.Drawing.Font("MS PGothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxRedundancia.FormattingEnabled = true;
            this.cbxRedundancia.Items.AddRange(new object[] {
            "/-/-/-/-",
            "NORMAL",
            "HIGH",
            "LOW"});
            this.cbxRedundancia.Location = new System.Drawing.Point(42, 311);
            this.cbxRedundancia.Name = "cbxRedundancia";
            this.cbxRedundancia.Size = new System.Drawing.Size(123, 28);
            this.cbxRedundancia.TabIndex = 8;
            this.cbxRedundancia.SelectedIndexChanged += new System.EventHandler(this.cbxRedundancia_SelectedIndexChanged);
            // 
            // btnCrear
            // 
            this.btnCrear.BackColor = System.Drawing.Color.DarkRed;
            this.btnCrear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCrear.Font = new System.Drawing.Font("MS PGothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrear.Location = new System.Drawing.Point(45, 419);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.btnCrear.Size = new System.Drawing.Size(126, 49);
            this.btnCrear.TabIndex = 9;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = false;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // Instalación
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(735, 539);
            this.Controls.Add(this.btnCrear);
            this.Controls.Add(this.cbxRedundancia);
            this.Controls.Add(this.cbxInstall);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.txtInformacion);
            this.Controls.Add(this.button1);
            this.Name = "Instalación";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtInformacion;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox cbxInstall;
        private System.Windows.Forms.ComboBox cbxRedundancia;
        private System.Windows.Forms.Button btnCrear;
    }
}

