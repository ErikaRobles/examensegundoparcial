﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen2Parcial
{
    public partial class Instalación : Form
    {
        private string[] input;
        private Dictionary<int, string> parsed;
        private bool ignorarComentarios = true;
        private string path = Path.Combine(Directory.GetCurrentDirectory(), "SegundoExamenParcial.txt");
        

        public Instalación()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            StreamReader stream = new StreamReader(path);
            string archivoDatos = stream.ReadToEnd();
            txtInformacion.Multiline = true;
            input = archivoDatos.Split(Environment.NewLine.ToCharArray()).ToArray();
            parsed = parseInput();
            txtInformacion.Text = printParsed();
            stream.Close();
            

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtInformacion.Clear();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            StreamReader stream = new StreamReader(path);
            string archivoDatos = stream.ReadToEnd();
            txtInformacion.Multiline = true;
            txtInformacion.Text = archivoDatos;
            stream.Close();
        }
        private Dictionary<int, string> parseInput()
        {
            
            Dictionary<int, string> output = new Dictionary<int, string>();
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].Length != 0 && input[i][0] != '#' )
                {
                    output.Add(i, input[i]);
                }
            }
            return output;
        }

        private String printParsed()
        {
            List<string> output = new List<string>();

            // Sort by line number
            var keys = parsed.Keys.ToList();
            keys.Sort();

            foreach (var key in keys)
            {
                output.Add(parsed[key]);
            }

            return string.Join(Environment.NewLine, output);

        }
       

        private void btnCrear_Click(object sender, EventArgs e)
        {
            
              var output = new List<string>();
            string[] lines = txtInformacion.Text.Split(Environment.NewLine.ToCharArray()).ToArray();
            if (!ignorarComentarios)
            {
                var userTextQ = new Queue<string>(lines);
                for (int i = 0; i < input.Length; i++)
                {
                    if (parsed.ContainsKey(i))
                    {
                        output.Add(userTextQ.Dequeue());
                    }
                    else
                    {
                        output.Add(input[i]);
                    }
                }
            }
            else
            {
                output = lines.ToList();
            }
          
            System.Diagnostics.Debug.WriteLine(string.Join(Environment.NewLine, output));
            System.IO.File.WriteAllText(@"C:\Users\erika\source\repos\SegundoExamenParcial.txt", string.Join(Environment.NewLine, output));
        }

        private void cbxInstall_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = cbxInstall.GetItemText(cbxInstall.SelectedItem);
            updateText(selectedValue, ".option");
        }

        private void updateText(string selectedValue, string wordToFind)
        {
            foreach (KeyValuePair<int, string> entry in parsed)
            {
                if (entry.Value.Contains(wordToFind))
                {
                    string newValue = appendSelectedToSentence(entry.Value, selectedValue);
                    parsed[entry.Key] = newValue;
                    txtInformacion.Text = printParsed();
                    break;
                }
            }
        }

        private string appendSelectedToSentence(string sentence, string selected)
        {
            //si hay más de un igual
            if (!sentence.Contains("=") || selected.TakeWhile(c => c == '=').Count() > 1)
            {
                return "";
            }
            StringBuilder sb = new StringBuilder(sentence);
            sb.Replace("=", "=" + selected);
            return sb.ToString();

        }


        private void cbxRedundancia_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selectedValue = cbxRedundancia.GetItemText(cbxRedundancia.SelectedItem);
            updateText(selectedValue, ".redundancia");
        }
    }
}
